package org.example;

import java.util.Arrays;
/**
 * The Sorting App class sorts a list of numbers in ascending order.
 */
public class sortingApp {
    /**
     * The entry point of the Sorting App program.
     *
     * @param args The command-line arguments representing the numbers to be sorted.
     */
    public  void main(String[] args) {
        if (args.length > 10) {
            System.out.println("Too many numnbers");
        }
        int[] num = new int[args.length];
        for (int a = 0; a < args.length; a++) {
            try {
                num[a] = Integer.parseInt(args[a]);
            } catch (NumberFormatException e) {
                System.out.println("Invalid numeral");
            }
        }


        Arrays.sort(num);

        for (int number : num) {
                System.out.print(number + " ");
            }
        }
    }


