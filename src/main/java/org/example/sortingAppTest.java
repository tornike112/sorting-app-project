package org.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class sortingAppTest {
    private final String[] input;
    private final int[] expected;

    public sortingAppTest(String[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, new int[]{}},                     // Corner case: empty input
                {new String[]{"1"}, new int[]{1}},                   // Corner case: single element
                {new String[]{"1", "3", "2"}, new int[]{1, 2, 3}},       // Nominal case
                {new String[]{String.valueOf(Integer.MAX_VALUE)}, new int[]{Integer.MAX_VALUE}},             // Corner case: maximum value
                {new String[]{String.valueOf(Integer.MAX_VALUE), String.valueOf(Integer.MIN_VALUE)}, new int[]{Integer.MIN_VALUE, Integer.MAX_VALUE}}, // Corner case: maximum and minimum values
        });
    }

    @Test
    public void testSortNumbers() {
        sortingApp sorting = new sortingApp();
        sorting.main(input);
        int[] actual = Arrays.stream(input).mapToInt(Integer::parseInt).toArray();
        Arrays.sort(actual);
        Assert.assertArrayEquals(expected, actual);
    }
}
